package onc.mmr.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Address {
	
	int id;
	String line1;
	String line2;
	
	String suburb;
	
	/**
	 * City or Town
	 */
	String city;
	
	/**
	 * post code, zip code
	 */
	String postalCode;
	String province;
	String country;
	
	

}
