package onc.mmr.user;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasicInfo {

	int id;
	String firstName;
	String midName;
	String lastName;
	Date DateOfBirth;
}
