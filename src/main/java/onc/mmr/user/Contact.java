package onc.mmr.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contact {

	int id;	
	String phoneOne;
	String phoneTwo;
	String mobileOne;
	String mobileTwo;
	String fax;
	String email;
	String website;	
}
