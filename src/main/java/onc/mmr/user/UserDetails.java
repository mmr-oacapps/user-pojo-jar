package onc.mmr.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class UserDetails {
	
	int id;	
	BasicInfo basicInfo;	
	Address address;	
	Contact contact;
	
	

}
